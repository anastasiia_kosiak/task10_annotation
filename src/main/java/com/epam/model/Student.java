package com.epam.model;

public class Student {
    @Print
    private String name;
    @Print
    private String lastName;
    private int age;

    public Student(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String howOld() {
        return "Student's age is " + age;
    }

    public boolean isAdult(int age) {
        return age >= 18;
    }

    public String myMethod(String a, int... args) {
        String info = "Name: " + a + "; Day of birth: ";
        for (int numbers : args) {
            info += numbers + " ";
        }
        return info;
    }

    public String myMethod(String... args) {
        String fullName = "Full name: ";
        for (String s : args) {
            fullName += s + " ";
        }
        return fullName;
    }

}
