package com.epam;

import com.epam.controller.Controller;
import com.epam.model.Student;

import com.epam.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;

public class Application {
    private static final Logger logger = LogManager.getLogger("InfoForUser");

    public static void main(String[] args) {
        Student person = new Student("Anna", "Vasylchuk", 19);
        Controller controller = new Controller(person);
        View myView = new View(controller);
        try {
            myView.printFields();
            myView.invokeMethods();
            myView.setValueNotKnowingType();
            myView.shoeSecretInfo(person);
        } catch (ClassNotFoundException e) {
            logger.info("Can`t find the class!!!");
        } catch (NoSuchMethodException e) {
            logger.info("Can`t find method.");
        } catch (IllegalAccessException e) {
            logger.info("Can`t access method.");
        } catch (InvocationTargetException e) {
            logger.info("Fail(");
        } catch (NoSuchFieldException e) {
            logger.info("Set true to field access");
        }
    }
}
