package com.epam.controller;

import com.epam.model.*;
import java.lang.reflect.Field;

public class Controller {
    private Student student;

    public Controller(Student student) {
        this.student = student;
    }

    public Student getPerson() {
        return student;
    }

    public Field[] getFieldsOfObject() {
        return student.getClass().getDeclaredFields();
    }

    public void changeValueOfFields() throws NoSuchFieldException, IllegalAccessException {
        Field name = student.getClass().getDeclaredField("name");
        Field lastName = student.getClass().getDeclaredField("lastName");
        Field age = student.getClass().getDeclaredField("age");
        name.setAccessible(true);
        lastName.setAccessible(true);
        age.setAccessible(true);
        name.set(student, "Anastasiia");
        lastName.set(student, "Kosiak");
        age.set(student, 18);
    }

    public String getSecretInfo(Object object) throws ClassNotFoundException {
        return new InfoWriter(object).getSecretInfo();
    }
}
